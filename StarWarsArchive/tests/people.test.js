const request = require('supertest')
const app = require('../app')

// FIXME? As of Feb 11th 2020, NPM request library has been deprecated?
//   SEE: https://www.npmjs.com/package/request

describe('GET /people', () => {
  it('should return all people given no query param "sortBy"', async () => {
    const res = await request(app)
      .get('/people')
    expect(res.statusCode).toEqual(200)
    expect(res.body.results.length).toBeGreaterThan(10)
    expect(res.body.results.length).toEqual(res.body.count)
  })

  // TODO Additional test cases to consider implementing...
  /*
  it('should sort results given query param "sortBy=name"', async () => {
    const res = await request(app)
      .get('/people?sortBy=name')
    expect(res.statusCode).toEqual(200)
    expect(false).toEqual('completed test case')
    //expect(res.body).toHaveProperty(keyPath, value?)
  })

  it('should sort results given query param "sortBy=height"', async () => {
    const res = await request(app)
      .get('/people?sortBy=height')
    expect(res.statusCode).toEqual(200)
    expect(false).toEqual('completed test case')
    //expect(res.body).toHaveProperty(keyPath, value?)
  })

  it('should sort results given query param "sortBy=mass"', async () => {
    const res = await request(app)
      .get('/people?sortBy=mass')
    expect(res.statusCode).toEqual(200)
    expect(false).toEqual('completed test case')
    //expect(res.body).toHaveProperty(keyPath, value?)
  })
  */
})

const request = require('supertest')
const app = require('../app')

// FIXME NPM request library has been deprecated as of Feb 11th 2020?
//   SEE https://www.npmjs.com/package/request

describe('GET /planets', () => {
  it('should list resources', async () => {
    const res = await request(app)
      .get('/planets')
    expect(res.statusCode).toEqual(200)
    //expect(res.body).toHaveProperty(keyPath, value?)
  })
})

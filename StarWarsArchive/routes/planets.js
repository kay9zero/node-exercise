const express   = require('express');
const axios     = require('axios');
const getPeople = require('../common/people');
const router    = express.Router();

// This block here might significantly increases server startup time?
var peopleCache;
getPeople()
  .then((data) => {
    peopleCache = { };
    data.results.forEach(e => (peopleCache[e.url] = e.name));
    console.log('Initialized peopleCache');
  })
  .catch((err) => {
    console.warn(err);
    throw 'Failed to init peopleCache';
  });

/* GET planets listing. */
router.get('/', async function(req, res, next) {

  var firstPage = await fetchPagePlanets(1);
  var count     = firstPage.count;
  var results   = firstPage.results;
  var perPage   = results.length;
  var maxPages  = Math.floor(count / perPage);
  if (count % perPage) {
    maxPages++;
  }

  getFullName('');
  firstPage.results.forEach(planet => {
    // TODO Ughh... What am I doing wrong here? This seems to not be transforming the array...
    //planet.residents.map(residentUrl => peopleCache[residentUrl]);
    // Resorting to this forEach hack (for meow)...
    let r = [];
    planet.residents.forEach(residentUrl => { r.push( peopleCache[residentUrl] ) });
    planet.residents = r;
    results.push(planet);
  });

  var promises = [ ];
  for (let pageNum = 2; pageNum <= maxPages; pageNum++) {
    promises.push(
      fetchPagePlanets(pageNum)
        .then((data) => {
          data.results.forEach(planet => {
            // TODO Ughh... What am I doing wrong here? This seems to not be transforming the array...
            //planet.residents.map(residentUrl => peopleCache[residentUrl]);
            // Resorting to this forEach hack (for meow)...
            let r = [];
            planet.residents.forEach(residentUrl => { r.push( peopleCache[residentUrl] ) });
            planet.residents = r;
            results.push(planet);
          });
        })
    );
  }

  await Promise.all(promises);

  res.json({
    count: count,
    results: results,
  });
});

function fetchPagePlanets (pageNum) {
  return axios
    .get('https://swapi.dev/api/planets?page=' + pageNum)
    .then((result) => {
      return result.data
    });
}

function getFullName (url) {
  fullName = peopleCache[url];
  return fullName || '<NOT_IN_CACHE>';
}

module.exports = router;

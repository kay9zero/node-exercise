const express = require('express');
const axios   = require('axios');
const router  = express.Router();

const getPeople = require('../common/people');

/* GET people listing. */
router.get('/', async function(req, res, next) {

  // XXX Have better error handling by using something like (especially when fetchPagePeople)...
  // res.status(500)
  // res.render('error', { error: err })

  var data = await getPeople();

  sortResults(
    data.results, req.query.sortBy || ''
  );

  res.json(data);
});

function sortResults (results, sortBy) {
  if(sortBy == 'name') {
    results.sort((a, b) => (a.name < b.name) ? -1 : (a.name > b.name) ? 1 : 0);
  }
  else if(sortBy == 'height') {
    results.sort((a, b) => (a.height < b.height) ? -1 : (a.height > b.height) ? 1 : 0);
  }
  else if(sortBy == 'mass') {
    // XXX better sort numerically than alphabetically?
    results.sort((a, b) => (a.mass < b.mass) ? -1 : (a.mass > b.mass) ? 1 : 0);
  }
  else {
    results.sort( (a, b) => {
      let aId = resourceId(a.url);
      let bId = resourceId(b.url);
      (aId < bId) ? -1 : (aId > bId) ? 1 : 0
    });
  }

  return results;
}

function resourceId (url) {
  var reMatch = url.match(/\/(\d)+\/$/);
  return reMatch[1];
}

module.exports = router;

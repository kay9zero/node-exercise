const axios = require('axios');

function fetchPagePeople (pageNum) {
  return axios
    .get('https://swapi.dev/api/people?page=' + pageNum)
    .then((result) => {
      return result.data
    })
    .catch((err) => {
      console.warn(err);
      throw 'Failed upstream GET /api/people';
    });
}

module.exports = async function(){

  var firstPage = await fetchPagePeople(1); // TODO .catch statement?
  var count     = firstPage.count;
  var results   = firstPage.results;
  var perPage   = results.length;
  var maxPages  = Math.floor(count / perPage);
  if (count % perPage) {
    maxPages++;
  }

  var promises = [ ];
  for (let pageNum = 2; pageNum <= maxPages; pageNum++) {
    promises.push(
      fetchPagePeople(pageNum)
        .then((data) => {
          // Not sure why concat isn't merging the above in so use forEach...
          // data.results.concat( nextPage.results );
          data.results.forEach(e => results.push(e));
        })
        // TODO .catch statement
    );
  }

  return Promise
    .all(promises)
    .then(() => {
      return {
        count:   count,
        results: results,
      }
    });
};
